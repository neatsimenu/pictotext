# pictotext

This project is done as a part of Software Development Academy Java course requirements. The software is capable of converting real images into symbol array reproducing similar looking image. The conversion is done by corelating parts of image with existing symbol.

## Getting Started

Source code is hosted here: https://gitlab.com/neatsimenu/pictotext.git

### Prerequisites

To run the project you need to install the MySQL database.
For development version 8.0.20 was used.

if MySQL is not present on your local machine please refer to dev.mysql.com isntructions on how to install it.
```
https://dev.mysql.com/doc/mysql-installation-excerpt/5.7/en/
```
```
use default credentials: username: root; password: root;
```
```
Run SQL dump file to crate required tables: https://gitlab.com/neatsimenu/pictotext/-/blob/master/sql_dump/pictotext_db_dump.sql
```
```
Run the project, use Java 1.8 version
```