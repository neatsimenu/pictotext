package com.ignas.pictotext.model;

import org.junit.Assert;
import org.junit.Test;

import java.awt.image.BufferedImage;

import static org.junit.Assert.*;

public class PictureTest {

    @Test
    public void resize_givenTwoPositiveDimensions_returnsBufferedImageWithCorrectDimensions(){
        //given
        int width = 600;
        int height = 400;
        BufferedImage image = new BufferedImage(500,500,BufferedImage.TYPE_3BYTE_BGR);

        //when
        BufferedImage newImage = Picture.resize(image,width,height);

        //then
        assertEquals(width,newImage.getWidth());
        assertEquals(height,newImage.getHeight());

    }
}
