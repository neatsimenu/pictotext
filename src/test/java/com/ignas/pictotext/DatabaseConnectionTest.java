package com.ignas.pictotext;

import org.junit.Test;
import com.ignas.pictotext.utils.PictotextDOM;
import com.ignas.pictotext.utils.TestClass;

import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;

public class DatabaseConnectionTest {

    @Test
    public void retrievingValuesFromDatabaseTest(){
        //given
        List<TestClass> testClasses = new ArrayList<>();
        String returnedValue = "";
        String expectedValue = "test";
        boolean isReturnedValueEqualToExpected = false;

        //when
        testClasses = PictotextDOM.findAllTestClassValues();
        returnedValue = testClasses.get(0).getTest();
        isReturnedValueEqualToExpected = returnedValue.contentEquals(expectedValue);
        //then

        assertTrue(isReturnedValueEqualToExpected);
    }
}
