package com.ignas.pictotext.service;

import com.sun.org.apache.xpath.internal.objects.XString;
import javafx.scene.control.CheckBox;
import org.apache.commons.math3.stat.correlation.PearsonsCorrelation;

import javax.imageio.ImageIO;
import java.awt.Checkbox;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Converter {
    static final private int incrementWidth = 10;
    static final private int incrementHeight = 15;
    static  final private int colorspace = BufferedImage.TYPE_3BYTE_BGR;

    static List<String> allSymbolList = Arrays.asList("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z","1","2","3","4","5","6","7","8","9","0", "dot","blank");
    static List<String> symbolList = new ArrayList<>();

    public  String convertPictureToString(BufferedImage inputPicture){
        return pictureToStringList(inputPicture).stream()
                .collect(Collectors.joining());
    }

    private static List<String> pictureToStringList(BufferedImage inputPicture) {
        int pictureWidth = inputPicture.getWidth();
        int pictureHeight = inputPicture.getHeight();
        int x = 0;
        int y = 0;
        List<String> result = new ArrayList<>();

        while(y < pictureHeight-incrementHeight){
            while (x < pictureWidth-incrementWidth) {
                BufferedImage croppedImage = createNewCroppedImage(inputPicture, x,y,incrementWidth,incrementHeight);
                String str = pictureToString(croppedImage);
                result.add(str);
                x += incrementWidth;
            }
            result.add("\n");
            y += incrementHeight;
            x = 0;
        }
        return result;
    }

    private static BufferedImage createNewCroppedImage(BufferedImage image, int x, int y, int width, int height){

        BufferedImage croppedImage = image.getSubimage(x,y,width,height);
        BufferedImage croppedResult = new BufferedImage(width,height, colorspace);

        Graphics2D graphics2D = croppedResult.createGraphics();
        graphics2D.drawImage(croppedImage, 0,0,null);
        graphics2D.dispose();

        return croppedResult;
    }

    private static String pictureToString(BufferedImage inputPicture) {
        String result = "";
        result = matchCroppedPictureToSymbol(inputPicture);
        return result;
    }

    private static String matchCroppedPictureToSymbol(BufferedImage input) {
        PearsonsCorrelation pearsonsCorrelation = new PearsonsCorrelation();
        String result = "";
        double correlationResult = -1;
        double[] croppedImageArray = pictureToDoubleArray(input);
        Map<String, double[]> symbolToArrayMap = new HashMap<String, double[]>();


        symbolList.forEach(s -> {
            BufferedImage symbolImage = null;
            BufferedImage symbolImageColorspaceApplied = null;
            try {
                symbolImage = ImageIO.read(new File("src/main/resources/symbolpictures/" + s + ".png"));
                symbolImageColorspaceApplied = createNewCroppedImage(symbolImage,0,0,symbolImage.getWidth(),symbolImage.getHeight());
            } catch (IOException e) {
                e.printStackTrace();
            }
            symbolToArrayMap.put(
                    s,
                    pictureToDoubleArray(symbolImageColorspaceApplied)
            );
        });

        for (String s : symbolList) {
            double[] symbolArray = symbolToArrayMap.get(s);
            double correlation = pearsonsCorrelation.correlation(symbolArray, croppedImageArray);
            System.out.println(s + " "+ correlation);
            if (correlation > correlationResult) {
                result = s;
                correlationResult = correlation;
            }
        }

        switch (result){
            case "dot":
                result = ".";
                break;
            case "blank":
                result = " ";
                break;
        }

        return result;
    }

    private static double[] pictureToDoubleArray(BufferedImage inputImage) {
        byte[] arr;
        double[] result;

        arr = ((DataBufferByte) inputImage.getRaster().getDataBuffer()).getData();
        result = toDoubleArray(arr);
        return result;
    }

    private static double[] toDoubleArray(byte[] byteArr) {

        byte[] byteArrTrasformed = addOneNumberToEndOfArrayIfArrayAllEqual(byteArr);

        double[] arr = new double[byteArrTrasformed.length];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = byteArrTrasformed[i];
        }
        return arr;
    }

    private static byte[] addOneNumberToEndOfArrayIfArrayAllEqual(byte[] input){
        boolean isAllEqual = true;
        for(int x = 1; x < input.length; x++){
            if(input[x-1] != input[x]){
                isAllEqual = false;
            }
        }
        if(isAllEqual){
            byte temp = (byte) (input[0]+1);
            input[0] = temp;
        }

        return input;
    }

    public static void takeListOfCheckedSymbols (List<CheckBox> checkboxList){

        checkboxList.forEach(
                checkbox -> {
                    if(checkbox.isSelected()){
                        symbolList.add(checkbox.getText());
                    }
                }
        );
    }

    public static List<String> getAllSymbolList()  {
        return allSymbolList;
    }
}
