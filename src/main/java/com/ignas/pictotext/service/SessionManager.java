package com.ignas.pictotext.service;

import com.ignas.pictotext.model.TextAsPicture;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import com.ignas.pictotext.utils.TestClass;


import java.util.Properties;

public class SessionManager {

    private static SessionFactory sessionFactory;

    public static SessionFactory getGetSessionFactory(){

        try{
            Configuration configuration = new Configuration();
            Properties settings = new Properties();

            settings.put(Environment.DRIVER, "com.mysql.jdbc.Driver");
            settings.put(Environment.URL, "jdbc:mysql://localhost:3306/pictotext");
            settings.put(Environment.USER, "root");
            settings.put(Environment.PASS, "root");
            settings.put(Environment.DIALECT, "org.hibernate.dialect.MySQL5Dialect");
            settings.put(Environment.SHOW_SQL, "false");
            settings.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");

            configuration.setProperties(settings);

            //Object mapping
            configuration.addAnnotatedClass(TestClass.class);
            configuration.addAnnotatedClass(TextAsPicture.class);

            ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                    .applySettings(configuration.getProperties()).build();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            sessionFactory.openSession();

        }catch (Exception e){
            System.out.println("failed to create session" + e);
        }

        return sessionFactory;
    }

}
