package com.ignas.pictotext;

import com.ignas.pictotext.model.Picture;
import com.ignas.pictotext.model.TextAsPicture;
import com.ignas.pictotext.service.Converter;
import com.ignas.pictotext.utils.PictotextDOM;
import com.sun.javafx.scene.layout.region.LayeredBackgroundPositionConverter;
import javafx.application.Application;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollBar;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.stage.FileChooser;
import javafx.stage.Stage;


import javax.imageio.ImageIO;
import java.awt.Desktop;
import java.awt.Scrollbar;
import java.awt.image.BufferedImage;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


public class Main extends Application {

    private Desktop desktop = Desktop.getDesktop();
    File file;
    Picture picture;
    ImageView imageView;
    Label pictureAsLabel;
    String pictureAsString="";
    List<CheckBox> checkBoxList;


    @Override
    public void start(Stage primaryStage) throws IOException {
        HBox rootHBox = new HBox(10);
        VBox inputPictureVBox = new VBox(10);
        inputPictureVBox.setPrefWidth(600);
        VBox outputLabelVBox = new VBox(10);
        outputLabelVBox.setPrefWidth(600);

        VBox galleryContent = new VBox();
        VBox menuContent = new VBox();
        GridPane symbolChooser = new GridPane();

        ScrollPane scrollPaneRight = new ScrollPane();
        scrollPaneRight.setPrefViewportWidth(320);
        ScrollPane scrollPaneLeft = new ScrollPane();
        scrollPaneLeft.setPrefViewportWidth(230);

        final FileChooser fileChooser = new FileChooser();
        final Button openFileButton = new Button("Open picture");
        final Button addToGalleryButton = new Button("Add to gallery");
        final TextField nameOfGalleryItem = new TextField("Add name");
        final Label pickSynblols = new Label("\nPick symbols to be used:");

        openFileButton.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        file = fileChooser.showOpenDialog(primaryStage);
                        if (file != null) {

                            picture = new Picture(file);
                            BufferedImage bufferedImage = picture.getBufferedImageResized();
                            Image image = SwingFXUtils.toFXImage(bufferedImage, null);
                            imageView.setImage(image);

                            pictureAsLabel = new Label();
                            Converter converter = new Converter();
                            converter.takeListOfCheckedSymbols(checkBoxList);
                            pictureAsLabel.setFont(new Font("Courier New", 16));
                            pictureAsLabel.setLineSpacing(-4);
                            pictureAsString = converter.convertPictureToString(bufferedImage);
                            pictureAsLabel.setText(pictureAsString);
                            outputLabelVBox.getChildren().clear();
                            outputLabelVBox.getChildren().add(pictureAsLabel);
                        }
                    }
                });

        addToGalleryButton.setOnAction(
                new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        PictotextDOM.addToGallery(nameOfGalleryItem.getText(),pictureAsString);
                        clearGallery(galleryContent);
                        drawGalleryItems(galleryContent);
                    }
                }
        );


        drawGalleryItems(galleryContent);

        scrollPaneLeft.setContent(menuContent);

        checkBoxList = createSymbolChooserCheckBoxList();
        constructSymbolChooserGridPane(symbolChooser, checkBoxList);

        menuContent.getChildren().addAll(openFileButton,nameOfGalleryItem,addToGalleryButton,pickSynblols,symbolChooser);

        imageView = new ImageView();
        inputPictureVBox.getChildren().add(imageView);

        scrollPaneRight.setContent(galleryContent);

        rootHBox.getChildren().addAll(scrollPaneLeft,inputPictureVBox,outputLabelVBox,scrollPaneRight);

        Scene scene = new Scene(rootHBox, 1770, 640);
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private static void drawGalleryItems(VBox galleryContent){
        List<TextAsPicture> textAsPictureList = new ArrayList<>();
        textAsPictureList = PictotextDOM.findAllTextAsPictureValues();

        textAsPictureList.forEach(
                textAsPicture -> {
                    Button deleteGaleryItem = new Button("Delete");
                    deleteGaleryItem.setOnAction(
                            new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    PictotextDOM.removeFromGallery(textAsPicture);
                                    clearGallery(galleryContent);
                                    drawGalleryItems(galleryContent);
                                }
                            }
                    );

                    Button openGaleryItem = new Button("Open");
                    openGaleryItem.setOnAction(
                            new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    HBox root = new HBox();

                                    Label galerryItemName = new Label(textAsPicture.getName());
                                    Label galleryItem = new Label(textAsPicture.getTextifiedPicture());
                                    galleryItem.setFont(new Font("Courier New", 16));
                                    galleryItem.setLineSpacing(-4);

                                    root.getChildren().add(galleryItem);

                                    Scene scene = new Scene(root, 600,600);
                                    Stage stage = new Stage();
                                    stage.setTitle(galerryItemName.getText());
                                    stage.setScene(scene);
                                    stage.show();
                                }
                            }
                    );

                    Button changeGaleryItemName = new Button("Change name");
                    changeGaleryItemName.setOnAction(
                            new EventHandler<ActionEvent>() {
                                @Override
                                public void handle(ActionEvent event) {
                                    VBox root = new VBox();

                                    Label galerryItemName = new Label("Old value -> " + textAsPicture.getName());
                                    TextField newValue = new TextField();
                                    HBox newValueBox = new HBox();
                                    Label newValueLabel = new Label("New value -> ");
                                    newValueBox.getChildren().addAll(newValueLabel,newValue);

                                    Button changeName = new Button("Change name");
                                    changeName.setOnAction(
                                            new EventHandler<ActionEvent>() {
                                                @Override
                                                public void handle(ActionEvent event) {
                                                    PictotextDOM.changeNameOfGalleryItem(textAsPicture,newValue.getText());
                                                    clearGallery(galleryContent);
                                                    drawGalleryItems(galleryContent);
                                                }
                                            }
                                    );

                                    root.getChildren().addAll(galerryItemName,newValueBox,changeName);

                                    Scene scene = new Scene(root, 300,100);
                                    Stage stage = new Stage();
                                    stage.setTitle("Change name");
                                    stage.setScene(scene);
                                    stage.show();
                                }
                            }
                    );

                    Label galerryItemName = new Label(textAsPicture.getName());
                    Label galleryItem = new Label(textAsPicture.getTextifiedPicture());
                    galleryItem.setFont(new Font("Courier New", 8));
                    galleryItem.setLineSpacing(-4);

                    HBox galleryButtons = new HBox();
                    galleryButtons.getChildren().addAll(deleteGaleryItem,openGaleryItem, changeGaleryItemName);

                    galleryContent.getChildren().addAll(galerryItemName, galleryItem,galleryButtons);
                }
        );
    }

    private static void clearGallery(VBox galleryContent){
        galleryContent.getChildren().clear();
    }

    private static List<CheckBox> createSymbolChooserCheckBoxList() {
        List<CheckBox> result = new ArrayList<>();
        for (String s : Converter.getAllSymbolList()) {
            result.add(new CheckBox(s));
        }
        return result;
    }

    private static void constructSymbolChooserGridPane(GridPane symbolChooser, List<CheckBox> checkBoxList){
        symbolChooser.setHgap(10);
        symbolChooser.setVgap(5);
        int columnIndex = 0;
        int rowIndex = 0;
        for(CheckBox checkBox : checkBoxList){
            symbolChooser.add(checkBox,columnIndex,rowIndex);
            if(columnIndex == 3){
                columnIndex=0;
                rowIndex++;
            }else{
                columnIndex++;
            }
        }
    }

    public static void main(String[] args) {
        Main.launch();
    }

}