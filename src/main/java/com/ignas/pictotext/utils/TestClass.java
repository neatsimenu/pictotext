package com.ignas.pictotext.utils;



import javax.persistence.*;

@Entity
@Table(name = "test")
public class TestClass {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    private String test;

    public String getTest() {
        return test;
    }

    public int getId(){
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTest(String test) {
        this.test = test;
    }
}
