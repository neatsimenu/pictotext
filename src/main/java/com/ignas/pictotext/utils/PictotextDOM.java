package com.ignas.pictotext.utils;

import com.ignas.pictotext.model.TextAsPicture;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import com.ignas.pictotext.service.SessionManager;

import java.util.ArrayList;
import java.util.List;

public class PictotextDOM {

    public static List<TestClass> findAllTestClassValues(){
        List<TestClass> testClass = new ArrayList<>();

        String selectAllHQL = "from TestClass";

        try(Session session = SessionManager.getGetSessionFactory().openSession()) {
            Query query = session.createQuery(selectAllHQL);
            testClass = query.list();

        }catch (Exception e){
            System.out.println(e);
        }
        return  testClass;
    }

    public static List<TextAsPicture> findAllTextAsPictureValues(){
        List<TextAsPicture> textAsPictureList = new ArrayList<>();

        String selectAllHQL = "from TextAsPicture";

        try(Session session = SessionManager.getGetSessionFactory().openSession()) {
            Query query = session.createQuery(selectAllHQL);
            textAsPictureList = query.list();

        }catch (Exception e){
            System.out.println(e);
        }
        return  textAsPictureList;
    }

    public static void addToGallery(String name, String textifiedPicture){
       TextAsPicture textAsPicture = new TextAsPicture();
       textAsPicture.setName(name);
       textAsPicture.setTextifiedPicture(textifiedPicture);

        try(Session session = SessionManager.getGetSessionFactory().openSession()) {

           session.save(textAsPicture);

        }catch (Exception e){
            System.out.println(e);
        }
    }

    public static void removeFromGallery(TextAsPicture textAsPicture){
        try(Session session = SessionManager.getGetSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            session.remove(textAsPicture);
            transaction.commit();

        }catch (Exception e){
            System.out.println(e);
        }
    }

    public static void changeNameOfGalleryItem(TextAsPicture textAsPicture, String newName){
        try(Session session = SessionManager.getGetSessionFactory().openSession()) {
            Transaction transaction = session.beginTransaction();
            textAsPicture.setName(newName);
            session.update(textAsPicture);
            transaction.commit();

        }catch (Exception e){
            System.out.println(e);
        }
    }

}
