package com.ignas.pictotext.model;

import javafx.scene.image.Image;

import javax.imageio.ImageIO;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.File;
import java.io.IOException;

public class Picture {
    static private final int width = 600;
    static private final int height = 600;

    BufferedImage bufferedImage;
    int colorspace = BufferedImage.TYPE_3BYTE_BGR;

    public Picture(File file) {
        this.bufferedImage = bufferedImage;

        try {
            bufferedImage = ImageIO.read(file);
        } catch (IOException e) {
        }
    }

    public BufferedImage getBufferedImageResized() {
       return resize(bufferedImage, width, height);
    }

    public static BufferedImage resize(BufferedImage inputImage,int width, int height){
        BufferedImage scaledImage = new BufferedImage(
                width, height, inputImage.getType());

        Graphics2D graphics2D = scaledImage.createGraphics();
        graphics2D.setRenderingHint(RenderingHints.KEY_INTERPOLATION,RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        graphics2D.drawImage(inputImage, 0, 0, width, height, null);
        graphics2D.dispose();

        return scaledImage;
    }

}
